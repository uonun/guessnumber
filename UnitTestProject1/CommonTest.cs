﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GuessNumber.Bot.Austin.Expert.Core;

namespace UnitTestProject1
{
    [TestClass]
    public class CommonTest
    {
        [TestMethod]
        public void GetFigures()
        {
            int tmp = 9875;
            var a = tmp / 1000;
            var b = tmp / 100 % 10;
            var c = tmp / 10 % 10;
            var d = tmp % 10;

            Assert.AreEqual(tmp, a * 1000 + b * 100 + c * 10 + d);
        }
    }
}
