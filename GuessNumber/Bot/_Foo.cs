﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuessNumber.Bot
{
    class _Foo : BaseBot
    {
        int n = 123;
        public _Foo()
        {
            Answer = 4567;
        }

        protected override int doGuess(string judgeResult)
        {
            do
            {
                n++;
            } while(Utils.ContainsSameFigures(n));

            return n;
        }
    }
}
