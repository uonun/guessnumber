﻿using GuessNumber;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class Utils
    {
        public static int[] GetFigures(this int answer)
        {
            var a = answer / 1000;
            var b = answer / 100 % 10;
            var c = answer / 10 % 10;
            var d = answer % 10;

            return new[] { a, b, c, d };
        }

        public static int[] GetAB(this string result)
        {
            Debug.Assert(!string.IsNullOrWhiteSpace(result) && result.Trim().Length == 4, "The parameter result must be something like \"?A?B\".");

            var tmp = result.Trim().ToUpper().Replace("A", "").Replace("B", "");
            int n;
            if(int.TryParse(tmp, out n))
                return new[] { n / 10, n % 10 };
            else
                throw new BizException("Can not get A/B from: " + result);
        }

        /// <summary>
        /// 保证各位不相同
        /// </summary>
        /// <param name="n"></param>
        /// <param name="targetNums"></param>
        /// <returns></returns>
        public static bool ContainsSameFigures(int n)
        {
            var nums = n.GetFigures();
            return nums.Distinct().Count() != 4;
        }
    }
}
