﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;

namespace GuessNumber.Bot
{
    public abstract class BaseBot : IJudge, IPlayer
    {
        /// <summary>
        /// 待猜数字
        /// </summary>
        public int Answer { get; internal set; }
        /// <summary>
        /// 对其他Bot给出的结果的判断次数
        /// </summary>
        public int JudgeCount { get; private set; }

        public Stopwatch GuessingElapsed { get; private set; }

        public BaseBot()
        {
            Init();
        }

        /// <summary>
        /// 生成一个4位正整数（首位可以是0，且每位的整数不相同）。
        /// </summary>
        /// <returns></returns>
        public int Init()
        {
            int result = 0;
            List<int> nums;
            int a, b, c, d;
            while (true)
            {
                result = new Random(DateTime.Now.Millisecond + this.GetHashCode()).Next(123, 9876);

                nums = new List<int>(4);

                a = result / 1000;
                nums.Add(a);

                b = (result - 1000 * a) / 100;
                if (nums.Contains(b)) continue;
                nums.Add(b);

                c = (result - 1000 * a - 100 * b) / 10;
                if (nums.Contains(c)) continue;
                nums.Add(c);

                d = result - 1000 * a - 100 * b - 10 * c;
                if (nums.Contains(d)) continue;

                break;
            }

            //if (result % 2 == 0)
            //    result = 135;

            this.Answer = result;
            this.JudgeCount = 0;
            this.GuessingElapsed = new Stopwatch();

            return result;
        }

        /// <summary>
        /// 为其他Bot猜测的数字给出 ?A?B 形式的评价结果
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string Judge(int number)
        {
            this.JudgeCount++;
            return doJudge(number);
        }

        /// <summary>
        /// 猜测根据其他 Bot 给出的评价结果，再次猜测数字
        /// </summary>
        /// <param name="judgeResult"></param>
        /// <returns></returns>
        public int Guess(string judgeResult)
        {
            int x;

            try
            {
                GuessingElapsed.Start();
                x = doGuess(judgeResult);
                GuessingElapsed.Stop();

                List<int> targetNums;
                if (!IsOk(x, out targetNums))
                {
                    throw new BizException("Each digit must not be same as others!") { Source = this.ToString() };
                }
            }
            catch (Exception ex)
            {
                GuessingElapsed.Stop();
                throw new BizException("Guessing failed：" + ex.ToString(), ex) { Source = this.ToString() };
            }

            return x;
        }

        /// <summary>
        /// 猜测根据其他 Bot 给出的评价结果，再次猜测数字
        /// </summary>
        /// <param name="judgeResult">传入的 ?A?B 形式的字符串</param>
        /// <returns></returns>
        protected virtual int doGuess(string judgeResult)
        {
            throw new NotImplementedException("This bot is not able to guess numbers.");
        }

        /// <summary>
        /// 为其他Bot猜测的数字给出 ?A?B 形式的评价结果
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        protected virtual string doJudge(int number)
        {
            var targetNums = new List<int>(number.GetFigures());
            var nums = new List<int>(this.Answer.GetFigures());

            int countA = (targetNums[0] == nums[0] ? 1 : 0)
                        + (targetNums[1] == nums[1] ? 1 : 0)
                        + (targetNums[2] == nums[2] ? 1 : 0)
                        + (targetNums[3] == nums[3] ? 1 : 0);

            int countB = 0;
            targetNums.ForEach(x =>
            {
                if (nums.Contains(x) && targetNums.IndexOf(x) != nums.IndexOf(x)) countB++;
            });

            return string.Format("{0}A{1}B", countA, countB);
        }

        private bool IsOk(int n, out List<int> targetNums)
        {
            var nums = n.GetFigures();
            if (!nums.Any(x => nums.Count(y => y == x) > 1))
            {
                targetNums = new List<int>(nums);
                return true;
            }

            targetNums = null;
            return false;
        }

    }
}
