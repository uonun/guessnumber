﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuessNumber.Bot.Austin.Expert.Core;

namespace GuessNumber.Bot.Austin.Expert
{
    class Impl : BaseBot
    {
        private readonly Engine _eng = new Engine();
        private int _lastGuess = 0;

        public Impl()
        {
            _eng.Load();
        }

        protected override int doGuess(string judgeResult)
        {
            int result = 123;
            if (string.IsNullOrEmpty(judgeResult))
            {
                result = 123;
                _eng.AddGuessed(result, judgeResult);
            }
            else
            {
                _eng.AddGuessed(_lastGuess, judgeResult);

                switch (judgeResult.ToUpper())
                {
                    case "0A0B":
                    case "0A1B":
                    case "0A2B":
                    case "0A3B":
                    case "0A4B":
                    case "1A0B":
                    case "1A1B":
                    case "1A2B":
                    case "1A3B":
                    case "2A0B":
                    case "2A1B":
                    case "2A2B":
                    case "3A0B":
                        result = _eng.GetNext();
                        break;
                    case "4A0B":
                        result = _lastGuess;
                        break;
                    default:
                    case "3A1B": //not possible
                        throw new Exception("An unexpected result found: " + judgeResult);
                }
            }

            _lastGuess = result;
            return _lastGuess;
        }
    }
}
