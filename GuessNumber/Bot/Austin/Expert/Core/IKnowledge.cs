﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Expert.Core
{
    public interface IKnowledge
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="history"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        Result Apply(Dictionary<int, string> history, ref Result result);

        //void Study(int lastAnswer, string lastJudgeResult, Result nextResult);
    }
}
