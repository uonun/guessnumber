﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GuessNumber.Bot.Austin.Expert.Core
{
    public class Engine
    {
        private readonly Collection<IKnowledge> _knowledges = new Collection<IKnowledge>();
        private readonly Dictionary<int, string> _guessed = new Dictionary<int, string>();
        private const string REPO_JSON_PATH = @"Bot\Austin\Expert\Asset\Repository.json";
        private Result _result = new Result();

        /// <summary>
        /// Load knowledges
        /// </summary>
        public void Load()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, REPO_JSON_PATH);
            Debug.Assert(File.Exists(path), "The file does not exists: " + path);

            using (var fs = new StreamReader(path, Encoding.UTF8))
            {
                var json = fs.ReadToEnd();
                var repo = JsonConvert.DeserializeObject<Repository>(json);
                Debug.Assert(repo != null && repo.Repos.Count > 0, "Deserialize error, can not load repository.");

                foreach (var r in repo.Repos)
                {
                    _knowledges.Add(new KnowledgeBase(r));
                }
            }
        }

        public int GetNext()
        {
            foreach (var k in _knowledges)
            {
                var r = k.Apply(_guessed, ref _result);
                if (r != null)
                {
                    if (r.Value > 0)
                        return r.Value;

                    _result.Exclude.Item1.AddRange(r.Exclude.Item1.Where(x => !_result.Exclude.Item1.Contains(x)));
                    _result.Exclude.Item2.AddRange(r.Exclude.Item2.Where(x => !_result.Exclude.Item2.Contains(x)));
                    _result.Exclude.Item3.AddRange(r.Exclude.Item3.Where(x => !_result.Exclude.Item3.Contains(x)));
                    _result.Exclude.Item4.AddRange(r.Exclude.Item4.Where(x => !_result.Exclude.Item4.Contains(x)));

                    var tmpItems = new Dictionary<int, int>(_result.Include.Item1);
                    foreach (var x in tmpItems)
                    {
                        if (_result.Include.Item1.ContainsKey(x.Key))
                            _result.Include.Item1[x.Key] += _result.Include.Item1[x.Key];
                    }

                    tmpItems = new Dictionary<int, int>(_result.Include.Item2);
                    foreach (var x in tmpItems)
                    {
                        if (_result.Include.Item2.ContainsKey(x.Key))
                            _result.Include.Item2[x.Key] += _result.Include.Item2[x.Key];
                    }

                    tmpItems = new Dictionary<int, int>(_result.Include.Item3);
                    foreach (var x in tmpItems)
                    {
                        if (_result.Include.Item3.ContainsKey(x.Key))
                            _result.Include.Item3[x.Key] += _result.Include.Item3[x.Key];
                    }

                    tmpItems = new Dictionary<int, int>(_result.Include.Item4);
                    foreach (var x in tmpItems)
                    {
                        if (_result.Include.Item4.ContainsKey(x.Key))
                            _result.Include.Item4[x.Key] += _result.Include.Item4[x.Key];
                    }
                }
            }

            foreach (var i in _result.Exclude.Item1) { _result.Include.Item1.Remove(i); }
            foreach (var i in _result.Exclude.Item2) { _result.Include.Item2.Remove(i); }
            foreach (var i in _result.Exclude.Item3) { _result.Include.Item3.Remove(i); }
            foreach (var i in _result.Exclude.Item4) { _result.Include.Item4.Remove(i); }

            Debug.Assert(_result.Include.Item1.Count > 0);
            Debug.Assert(_result.Include.Item2.Count > 0);
            Debug.Assert(_result.Include.Item3.Count > 0);
            Debug.Assert(_result.Include.Item4.Count > 0);

            var tmp = GetMostPossibleResult(_result);
            int n = 0;
            int round = 0;
            while (_guessed.ContainsKey(tmp))
            {
                if (round > 10)
                    tmp = GetMostPossibleResult(_result, n++);
                else
                    tmp = GetRandomResult(_result);
            }

            return tmp;
        }

        private int GetMostPossibleResult(Result r, int skip = 0)
        {
            var A = (from x in r.Include.Item1
                     orderby x.Value descending
                     select x.Key).ToList();

            var B = (from x in r.Include.Item2
                     orderby x.Value descending
                     select x.Key).ToList();

            var C = (from x in r.Include.Item3
                     orderby x.Value descending
                     select x.Key).ToList();

            var D = (from x in r.Include.Item4
                     orderby x.Value descending
                     select x.Key).ToList();

            return GetResult(A, B, C, D, skip);
        }

        private int GetRandomResult(Result r)
        {
            var A = (from x in r.Include.Item1
                     orderby new Random((int)DateTime.Now.Ticks + x.Value).Next() descending
                     select x.Key).ToList();

            var B = (from x in r.Include.Item2
                     orderby new Random((int)DateTime.Now.Ticks + x.Value).Next() descending
                     select x.Key).ToList();

            var C = (from x in r.Include.Item3
                     orderby new Random((int)DateTime.Now.Ticks + x.Value).Next() descending
                     select x.Key).ToList();

            var D = (from x in r.Include.Item4
                     orderby new Random((int)DateTime.Now.Ticks + x.Value).Next() descending
                     select x.Key).ToList();

            return GetResult(A, B, C, D);
        }

        private int GetResult(List<int> a, List<int> b, List<int> c, List<int> d, int skip = 0)
        {
            for (int i = 0; i < a.Count; i++)
            {
                for (int j = 0; j < b.Count; j++)
                {
                    if (a[i] == b[j])
                    {
                        if (j == b.Count - 1)
                            break;
                        else
                            continue;
                    }

                    for (int k = 0; k < c.Count(); k++)
                    {
                        if (a[i] == c[k] || b[j] == c[k])
                        {
                            if (k == c.Count - 1)
                                break;
                            else
                                continue;
                        }

                        for (int l = 0; l < d.Count(); l++)
                        {
                            if (a[i] == d[l] || b[j] == d[l] || c[k] == d[l])
                            {
                                if (l == d.Count - 1)
                                    break;
                                else
                                    continue;
                            }

                            if (skip-- == 0)
                            {
                                return a[i] * 1000 + b[j] * 100 + c[k] * 10 + d[l];
                            }
                            else
                            {
                                if (l == d.Count - 1)
                                    break;
                                else
                                    continue;
                            }
                        }
                    }
                }
            }

            Debug.Fail("Can not get all 4 figures.");
            return 0;
        }

        public void AddGuessed(int lastGuess, string judgeResult)
        {
            _guessed[lastGuess] = judgeResult;
        }
    }
}
