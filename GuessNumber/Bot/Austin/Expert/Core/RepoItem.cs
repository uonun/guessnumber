﻿namespace GuessNumber.Bot.Austin.Expert.Core
{
    public class RepoItem
    {
        public int LastAnswer { get; set; }
        public int A { get; set; }
        public int B { get; set; }
        /// <summary>
        /// The type and assembly for getting next answer.
        /// </summary>
        public string Action { get; set; }
        public int NextAnswer { get; set; }
    }
}