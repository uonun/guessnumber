﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Expert.Core
{
    public class KnowledgeBase : IKnowledge
    {
        private RepoItem r;

        public KnowledgeBase(RepoItem r)
        {
            this.r = r;
        }

        public virtual Result Apply(Dictionary<int, string> history, ref Result result)
        {
            if (r.NextAnswer != 0)
            {
                result.Value = r.NextAnswer;
            }
            else
            {
                Consider0B(history, 3, ref result);
                Consider0B(history, 2, ref result);

                // are they useful?
                ConsiderA(history, 3, ref result);
                ConsiderA(history, 2, ref result);
                ConsiderA(history, 1, ref result);

                // TODO: use history to get next result.
                int[] lastabcd = null;
                int lastTotalN = 0;
                foreach (var h in history)
                {
                    if (string.IsNullOrWhiteSpace(h.Value))
                        continue;

                    var AB = h.Value.GetAB();
                    var abcd = h.Key.GetFigures();

                    // CHECK A, B.
                    // When A=0, it means B=4, so, if AB[0], there must be AB[1]=4.
                    if (AB[0] == 0)
                    {
                        Not(0, abcd[0], ref result);
                        Not(1, abcd[1], ref result);
                        Not(2, abcd[2], ref result);
                        Not(3, abcd[3], ref result);
                    }

                    // CHECK A+B
                    var totalN = AB[0] + AB[1];

                    if (totalN < lastTotalN && lastabcd != null)
                    {
                        var x = lastabcd.Intersect(abcd);
                        foreach (var i in x)
                        {
                            Maybe(0, i, 2 * totalN, ref result);
                            Maybe(1, i, 2 * totalN, ref result);
                            Maybe(2, i, 2 * totalN, ref result);
                            Maybe(3, i, 2 * totalN, ref result);
                        }
                    }
                    lastTotalN = totalN;
                    lastabcd = abcd;

                    if (totalN == 4)
                    {
                        foreach (var i in abcd)
                        {
                            Maybe(0, i, 100, ref result);
                            Maybe(1, i, 100, ref result);
                            Maybe(2, i, 100, ref result);
                            Maybe(3, i, 100, ref result);
                        }

                        var others = Result.DIC.Where(x => !abcd.Contains(x)).AsEnumerable();
                        foreach (var other in others)
                        {
                            result.Include.Item1.Remove(other);
                            result.Include.Item2.Remove(other);
                            result.Include.Item3.Remove(other);
                            result.Include.Item4.Remove(other);
                        }

                        result.Exclude.Item1.AddRange(others);
                        result.Exclude.Item2.AddRange(others);
                        result.Exclude.Item3.AddRange(others);
                        result.Exclude.Item4.AddRange(others);
                    }
                    else if (totalN == 3)
                    {
                        foreach (var i in abcd)
                        {
                            Maybe(0, i, 5, ref result);
                            Maybe(1, i, 5, ref result);
                            Maybe(2, i, 5, ref result);
                            Maybe(3, i, 5, ref result);
                        }
                    }
                    else if (totalN == 2)
                    {
                        Func<Dictionary<int, string>, int, int> getFirst = (his, n) =>
                        {
                            return his.Where(x =>
                            {
                                var tmpAB = x.Value.GetAB();
                                return x.Key != h.Key && tmpAB[0] + tmpAB[1] > n &&
                                       x.Key.GetFigures().Intersect(abcd).Count() > 2;
                            }).Select(y => y.Key).FirstOrDefault();
                        };

                        var priority = 5;
                        var tmp = getFirst(history, 3);
                        if (tmp == 0)
                        {
                            tmp = getFirst(history, 2);
                            priority = 3;
                        }

                        if (tmp == 0)
                        {
                            tmp = getFirst(history, 1);
                            priority = 1;
                        }

                        if (tmp != 0)
                        {
                            var abcd2 = tmp.GetFigures().Intersect(abcd);
                            foreach (var i in abcd2)
                            {
                                if (abcd[0] == i) { Maybe(0, i, priority, ref result); continue; }
                                if (abcd[1] == i) { Maybe(1, i, priority, ref result); continue; }
                                if (abcd[2] == i) { Maybe(2, i, priority, ref result); continue; }
                                if (abcd[3] == i) { Maybe(3, i, priority, ref result); continue; }
                            }
                        }
                    }
                    else if (totalN == 1)
                    {
                        var others = Result.DIC.Where(x => !abcd.Contains(x)).AsEnumerable();
                        foreach (var other in others)
                        {
                            Maybe(0, other, 1, ref result);
                            Maybe(1, other, 1, ref result);
                            Maybe(2, other, 1, ref result);
                            Maybe(3, other, 1, ref result);
                        }
                    }
                    else
                    {
                        foreach (var i in abcd)
                        {
                            result.Include.Item1.Remove(i);
                            result.Include.Item2.Remove(i);
                            result.Include.Item3.Remove(i);
                            result.Include.Item4.Remove(i);
                        }

                        result.Exclude.Item1.AddRange(abcd);
                        result.Exclude.Item2.AddRange(abcd);
                        result.Exclude.Item3.AddRange(abcd);
                        result.Exclude.Item4.AddRange(abcd);
                    }
                }

                /*
                 * consider all ?A0B
                 * 
                var q = history.Where(x => x.Value.GetAB()[1] == 0).Select(y => y.Key).ToList();
                if (q.Count > 0)
                {
                    var first = 0;
                    do
                    {
                        first = history.Where(x => x.Value.GetAB()[0] > 2).Select(y => y.Key).FirstOrDefault();
                        if (first != 0) break;

                        first = history.Where(x => x.Value.GetAB()[0] > 1).Select(y => y.Key).FirstOrDefault();
                        if (first != 0) break;

                        first = history.Where(x => x.Value.GetAB()[0] > 0).Select(y => y.Key).FirstOrDefault();
                        if (first != 0) break;

                    } while (false);
                    var abcd = first.GetFigures();

                    var abcdIntersect = abcd.AsEnumerable();
                    for (int n = 1; n < q.Count; n++)
                    {
                        var tmp = abcdIntersect.Intersect(q[n].GetFigures());
                        if (tmp.Any())
                        {
                            abcdIntersect = tmp;

                            foreach (var i in abcdIntersect)
                            {
                                if (result.Include.Item1.ContainsKey(i)) result.Include.Item1[i] += 100;
                                if (result.Include.Item2.ContainsKey(i)) result.Include.Item2[i] += 100;
                                if (result.Include.Item3.ContainsKey(i)) result.Include.Item3[i] += 100;
                                if (result.Include.Item4.ContainsKey(i)) result.Include.Item4[i] += 100;
                            }
                        }
                        else
                        {
                            abcdIntersect = abcd;
                        }
                    }
                }
                 */
            }
            return result;
        }

        /// <summary>
        /// ?A
        /// </summary>
        /// <param name="history"></param>
        /// <param name="nA"></param>
        /// <param name="result"></param>
        private void ConsiderA(Dictionary<int, string> history, int nA, ref Result result)
        {
            var q = history.Where(x => x.Value.GetAB()[0] == nA).Select(y => y.Key).ToList();
            if (q.Count > 0)
            {
                var priority = nA == 3 ? 5 : (nA == 2 ? 1 : -1);

                for (int m = 0; m < q.Count; m++)
                {
                    for (int n = 0; n < q.Count; n++)
                    {
                        if (m == n) continue;

                        var abcd = q[m].GetFigures();
                        var tmp = q[n].GetFigures().Intersect(abcd);
                        if (tmp.Any())
                        {
                            foreach (var i in tmp)
                            {
                                if (abcd[0] == i) { Maybe(0, i, priority, ref result); continue; }
                                if (abcd[1] == i) { Maybe(1, i, priority, ref result); continue; }
                                if (abcd[2] == i) { Maybe(2, i, priority, ref result); continue; }
                                if (abcd[3] == i) { Maybe(3, i, priority, ref result); continue; }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 3A0B, 2A0B
        /// </summary>
        /// <param name="history"></param>
        /// <param name="nA"></param>
        /// <param name="result"></param>
        private void Consider0B(Dictionary<int, string> history, int nA, ref Result result)
        {
            var q = history.Where(x =>
            {
                var AB = x.Value.GetAB();
                return AB[0] == nA && AB[1] == 0;
            }).Select(y => y.Key).ToList();

            if (q.Count > 0)
            {
                for (int m = 0; m < q.Count; m++)
                {
                    for (int n = 0; n < q.Count; n++)
                    {
                        if (m == n) continue;

                        var abcd = q[m].GetFigures();
                        var abcd2 = q[n].GetFigures();
                        var tmp = abcd2.Intersect(abcd).Where(x =>
                        {
                            return (abcd[0] == x && abcd2[0] == x)
                                || (abcd[1] == x && abcd2[1] == x)
                                || (abcd[2] == x && abcd2[2] == x)
                                || (abcd[3] == x && abcd2[3] == x);
                        });

                        if (nA == 3)
                        {
                            if (tmp.Any())
                            {
                                foreach (var i in tmp)
                                {
                                    if (abcd[0] == i) { Sure(0, i, ref result); continue; }
                                    if (abcd[1] == i) { Sure(1, i, ref result); continue; }
                                    if (abcd[2] == i) { Sure(2, i, ref result); continue; }
                                    if (abcd[3] == i) { Sure(3, i, ref result); continue; }
                                }
                            }
                        }
                        else if (nA == 2)
                        {
                            if (tmp.Count() > 1)
                            {
                                foreach (var i in tmp)
                                {
                                    if (abcd[0] == i) { Maybe(0, i, 100, ref result); continue; }
                                    if (abcd[1] == i) { Maybe(1, i, 100, ref result); continue; }
                                    if (abcd[2] == i) { Maybe(2, i, 100, ref result); continue; }
                                    if (abcd[3] == i) { Maybe(3, i, 100, ref result); continue; }
                                }
                            }
                            else if (tmp.Count() == 1)
                            {
                                var i = tmp.First();
                                if (abcd[0] == i) { Sure(0, i, ref result); continue; }
                                if (abcd[1] == i) { Sure(1, i, ref result); continue; }
                                if (abcd[2] == i) { Sure(2, i, ref result); continue; }
                                if (abcd[3] == i) { Sure(3, i, ref result); continue; }
                            }
                        }
                        else if (nA == 1)
                        {
                            Debug.Fail("Makes no sense.");
                        }
                    }
                }
            }
        }

        private void Not(int idx, int n, ref Result result)
        {
            switch (idx)
            {
                case 0:
                    result.Include.Item1.Remove(n);
                    result.Exclude.Item1.Add(n);
                    break;
                case 1:
                    result.Include.Item2.Remove(n);
                    result.Exclude.Item2.Add(n);
                    break;
                case 2:
                    result.Include.Item3.Remove(n);
                    result.Exclude.Item3.Add(n);
                    break;
                case 3:
                    result.Include.Item4.Remove(n);
                    result.Exclude.Item4.Add(n);
                    break;
            }
        }

        private void Sure(int idx, int n, ref Result result)
        {
            var others = Result.DIC.Where(x => x != n).AsEnumerable();

            switch (idx)
            {
                case 0:
                    result.Include.Item1.Clear();
                    result.Include.Item1.Add(n, 1);
                    result.Exclude.Item1.Clear();
                    result.Exclude.Item1.AddRange(others);
                    break;
                case 1:
                    result.Include.Item2.Clear();
                    result.Include.Item2.Add(n, 1);
                    result.Exclude.Item2.Clear();
                    result.Exclude.Item2.AddRange(others);
                    break;
                case 2:
                    result.Include.Item3.Clear();
                    result.Include.Item3.Add(n, 1);
                    result.Exclude.Item3.Clear();
                    result.Exclude.Item3.AddRange(others);
                    break;
                case 3:
                    result.Include.Item4.Clear();
                    result.Include.Item4.Add(n, 1);
                    result.Exclude.Item4.Clear();
                    result.Exclude.Item4.AddRange(others);
                    break;
            }
        }

        private void Maybe(int idx, int n, int priority, ref Result result)
        {
            switch (idx)
            {
                case 0:
                    if (result.Include.Item1.ContainsKey(n)) { result.Include.Item1[n] += priority; }
                    break;
                case 1:
                    if (result.Include.Item2.ContainsKey(n)) { result.Include.Item2[n] += priority; }
                    break;
                case 2:
                    if (result.Include.Item3.ContainsKey(n)) { result.Include.Item3[n] += priority; }
                    break;
                case 3:
                    if (result.Include.Item4.ContainsKey(n)) { result.Include.Item4[n] += priority; }
                    break;
            }
        }
    }
}
