﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Expert.Core
{
    public class Result
    {
        public static readonly int[] DIC = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        /// <summary>
        /// 答案的可能值。每一位数字的Dictionary集合中，Key表示数字本身，Value表示该数字的可能权重。
        /// </summary>
        public Tuple<Dictionary<int, int>, Dictionary<int, int>, Dictionary<int, int>, Dictionary<int, int>> Include
        {
            get
            {
                return _include;
            }
        }

        private Tuple<Dictionary<int, int>, Dictionary<int, int>, Dictionary<int, int>, Dictionary<int, int>> _include;

        /// <summary>
        /// The answer must not be.
        /// </summary>
        public Tuple<List<int>, List<int>, List<int>, List<int>> Exclude { get; private set; }

        public int Value { get; set; }

        public Result()
        {
            _include = new Tuple<Dictionary<int, int>, Dictionary<int, int>, Dictionary<int, int>, Dictionary<int, int>>(
                new Dictionary<int, int>(),
                new Dictionary<int, int>(),
                new Dictionary<int, int>(),
                new Dictionary<int, int>()
                );

            foreach (var i in DIC)
            {
                _include.Item1.Add(i, 0);
                _include.Item2.Add(i, 0);
                _include.Item3.Add(i, 0);
                _include.Item4.Add(i, 0);
            }

            Exclude = new Tuple<List<int>, List<int>, List<int>, List<int>>(
                new List<int>(),
                new List<int>(),
                new List<int>(),
                new List<int>());
        }
    }
}
