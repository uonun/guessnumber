﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Neural
{
    public class Figure
    {
        public int Value;
        public double? Possibility;
    }
}
