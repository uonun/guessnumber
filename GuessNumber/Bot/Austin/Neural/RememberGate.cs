﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Neural
{
    public class Remember
    {
        private List<Guess> _guesseds = new List<Guess>();
        public List<Guess> Guesseds => _guesseds;
    }
}
