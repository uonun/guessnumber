﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Neural.L1
{
    public class Input_L1
    {
        public int? Number { get; set; }
        public AB PreResult { get; set; }
    }
}
