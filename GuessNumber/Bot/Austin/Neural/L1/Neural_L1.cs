﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Neural.L1
{
    public class Neural_L1
    {
        private Cell[] _cells;
        private Dictionary<int, Dictionary<int, double?>> _dic;
        public int[] Confirmed { get; private set; }

        public Dictionary<int, Dictionary<int, double?>> Dic => _dic;
        public Cell[] Cells => _cells;

        public Neural_L1()
        {
            _cells = new Cell[4] { new Cell(0, this), new Cell(1, this), new Cell(2, this), new Cell(3, this) };

            _dic = new Dictionary<int, Dictionary<int, double?>>();
            for(int i = 0; i < 4; i++)
            {
                var list = new Dictionary<int, double?>();
                for(int j = 0; j < 10; j++)
                {
                    list.Add(j, null);
                }
                _dic.Add(i, list);
            }
        }

        public int[] Excite(Input_L1 input)
        {
            Debug.Assert(input.Number != null && input.PreResult != null);

            var nums = new int[4] {
                        _cells[0].Confirmed.HasValue?_cells[0].Confirmed.Value:-1,
                        _cells[1].Confirmed.HasValue?_cells[1].Confirmed.Value:-1,
                        _cells[2].Confirmed.HasValue?_cells[2].Confirmed.Value:-1,
                        _cells[3].Confirmed.HasValue?_cells[3].Confirmed.Value:-1,
                    };

            if(nums.Any(x => x == -1))
            {
                for(int i = 0; i < _cells.Length; i++)
                {
                    nums[i] = _cells[i].Excite(input);
                }
            }
            else
            {
                Confirmed = nums;
            }

            return nums;
        }
    }
}
