﻿#define 保证每一位都能取到数字

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Neural.L2
{
    public class Neural_L2
    {
        public int Excite(AB rst, Cell[] cells)
        {
            Debug.Assert(cells != null && cells.Length == 4);

            int[] nums;

            var all = cells[0].Posibles
                .Union(cells[1].Posibles)
                .Union(cells[2].Posibles)
                .Union(cells[3].Posibles);
            Debug.Assert(all.Count() >= 4);

            if(all.Count() == 4)
            {
                if(cells.All(x => x.Confirmed.HasValue))
                {
                    nums = all.ToArray();
                }
                else
                {
                    // 临时随机组合
                    nums = all.OrderBy(x => Guid.NewGuid().GetHashCode()).ToArray();
                }
            }
            else
            {
                nums = new int[4] {
                       cells[0].Confirmed.HasValue?cells[0].Confirmed.Value:-1,
                       cells[1].Confirmed.HasValue?cells[1].Confirmed.Value:-1,
                       cells[2].Confirmed.HasValue?cells[2].Confirmed.Value:-1,
                       cells[3].Confirmed.HasValue?cells[3].Confirmed.Value:-1,
                    };

#if 保证每一位都能取到数字
                // 整理优化
                for(int i = 0; i < 4; i++)
                {
                    for(int j = 0; j < 4; j++)
                    {
                        if(i == j)
                            continue;

                        var pa = new List<int>(cells[i].Posibles);
                        var pb = cells[j].Posibles;

                        // 两个的 Posibles 相等的？
                        if(pa.Count == 2 && pa.All(x => pb.Contains(x)) && pb.All(x => pa.Contains(x)))
                        {
                            nums[i] = cells[i].Posibles.First();
                            nums[j] = cells[j].Posibles.Except(new[] { nums[i] }).First();

                            Debug.Assert(nums[i] != nums[j]);
                        }
                    }
                }

                // 整理优化
                for(int i = 0; i < 4; i++)
                {
                    if(cells[i].Confirmed.HasValue)
                    {
                        for(int j = 0; j < 4; j++)
                        {
                            if(i == j)
                                continue;

                            cells[j].Except(new[] { cells[i].Confirmed.Value });
                        }
                    }
                }

                // 为了保证每一位都能取到数字，此处的实现逻辑太难看。。
                for(int m = 0; m < cells[3].Posibles.Length; m++)
                {
                    for(int n = 0; n < cells[2].Posibles.Length; n++)
                    {
                        for(int g = 0; g < cells[1].Posibles.Length; g++)
                        {
                            for(int h = 0; h < cells[0].Posibles.Length; h++)
                            {
                                var tmpnums = new int[] { -1, -1, -1, -1 };
                                nums = new int[4] {
                                   cells[0].Confirmed.HasValue?cells[0].Confirmed.Value:-1,
                                   cells[1].Confirmed.HasValue?cells[1].Confirmed.Value:-1,
                                   cells[2].Confirmed.HasValue?cells[2].Confirmed.Value:-1,
                                   cells[3].Confirmed.HasValue?cells[3].Confirmed.Value:-1,
                                };
                                for(int i = 0; i < cells.Length; i++)
                                {
                                    if(nums[i] > -1)
                                    {
                                        tmpnums[i] = nums[i];
                                        continue;
                                    }

                                    var skip = i == 3 ? m : i == 2 ? n : i == 1 ? g : h;
                                    var p = cells[i].Posibles.Skip(skip).Except(tmpnums);
                                    if(p.Any())
                                    {
                                        nums[i] = p.First();
                                        tmpnums[i] = nums[i];
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                if(nums.All(x => x > -1))
                                    break;
                            }

                            if(nums.All(x => x > -1))
                                break;
                        }

                        if(nums.All(x => x > -1))
                            break;
                    }

                    if(nums.All(x => x > -1))
                        break;
                }
#else
                var tmpnums = new int[] { -1, -1, -1, -1 };
                for(int i = 0; i < cells.Length; i++)
                {
                    if(nums[i] > -1)
                    {
                        tmpnums[i] = nums[i];
                        continue;
                    }

                    var p = cells[i].Posibles.Except(tmpnums);
                    if(p.Any())
                    {
                        nums[i] = p.First();
                        tmpnums[i] = nums[i];
                    }
                    else
                    {
                        break;
                    }
                }

                // 触发这个情况，一般都是数字比较确定，但是总被不正确的位置占用的情况
                // 比如第一位可能是 1、2、7，第四位可能是 1、2，则很可能第一位取到 1 或 2，使得第四位总取不到值
                // 因此需要考虑相互之间的位置约束关系
                if(nums.Any(x => x == -1))
                {
                    // TODO
                }
#endif
                Debug.Assert(nums.All(x => x > -1));
            }

            return nums[0] * 1000 + nums[1] * 100 + nums[2] * 10 + nums[3];
        }
    }
}
