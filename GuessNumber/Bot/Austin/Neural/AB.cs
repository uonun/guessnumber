﻿namespace GuessNumber.Bot.Austin.Neural
{
    public class AB
    {
        public int A { get; set; }
        public int B { get; set; }

        public override string ToString()
        {
            return $"{A}A{B}B";
        }
    }
}
