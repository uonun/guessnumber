﻿using GuessNumber.Bot.Austin.Neural.L1;
using GuessNumber.Bot.Austin.Neural.L2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Neural
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// 本想以神经网络+学习的方式来做，然而并未能如愿。实际上还是以权重的思想来实现的，与 Expert 一致。
    /// 区别仅仅在于，Expert 扁平化考虑四位的可能性，而本实现独立考虑每一位的可能性，然后在二级“Neural”里再考虑组合
    /// </remarks>
    public class NeuralNetwork : BaseBot
    {
        private Neural_L1 _neural1 = new Neural_L1();
        private Neural_L2 _neural2 = new Neural_L2();
        private Remember _remember = new Remember();
        private int _lastNum = 0;

        protected override int doGuess(string judgeResult)
        {
            var rst = 123;
            if(!string.IsNullOrEmpty(judgeResult))
            {
                var input = new Input_L1();
                input.Number = _lastNum;
                var ab = Utils.GetAB(judgeResult);
                input.PreResult = new AB()
                {
                    A = ab[0],
                    B = ab[1]
                };

                if(_remember.Guesseds.Any())
                    _remember.Guesseds.Last().CurrentResult = input.PreResult;

                while(true)
                {
                    var nums = _neural1.Excite(input);
                    rst = _neural2.Excite(input.PreResult, _neural1.Cells);

                    var guess = _remember.Guesseds.FirstOrDefault(x => x.Number == rst);
                    if(guess!=null)
                    {
                        input.Number = guess.Number;
                        input.PreResult = guess.CurrentResult;
                        continue;
                    }

                    if(!Utils.ContainsSameFigures(rst))
                        break;
                }

                _remember.Guesseds.Add(new Guess { Number = rst, PreResult = input.PreResult });

            }

            _lastNum = rst;
            return rst;
        }
    }
}
