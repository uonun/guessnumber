﻿using GuessNumber.Bot.Austin.Neural.L1;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Bot.Austin.Neural
{
    public class Cell
    {
        private int _idx;
        private Neural_L1 _neural;

        public int Idx => _idx;

        public int[] Posibles
        {
            get
            {
                var items = _neural.Dic[_idx]
                    .Where(x => x.Value == null || x.Value >= 0)
                    .OrderByDescending(x => x.Value)
                    .Select(x => x.Key)
                    .ToArray();
                return items;
            }
        }

        public int? Confirmed
        {
            get
            {
                var items = Posibles;
                return items.Length == 1 ? (int?)items.First() : null;
            }
            set
            {
                Debug.Assert(value.HasValue);

                for(int i = 0; i < 10; i++)
                {
                    _neural.Dic[_idx][i] = -9999999;
                }
                _neural.Dic[_idx][value.Value] = 1;
            }
        }

        public Cell(int idx, Neural_L1 neural)
        {
            _idx = idx;
            _neural = neural;
        }

        public int Excite(Input_L1 input)
        {
            var c = Confirmed;
            if(c.HasValue)
                return c.Value;

            Debug.Assert(input.Number != null);
            Debug.Assert(input.PreResult != null);

            var figures = Utils.GetFigures(input.Number.Value);
            var n = figures[_idx];

            // 排除所在位置的值
            if(input.PreResult.A == 0)
            {
                Except(new[] { n });
            }

            // 排除所在位置的值
            if(input.PreResult.A + input.PreResult.B == 0)
            {
                Except(figures);
            }
            else if(input.PreResult.A + input.PreResult.B == 4)
            {
                // 排除所在位置的其他值
                Except(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }.Except(figures));
            }

            Debug.Assert(Posibles.Length > 0);

            if(_neural.Dic[_idx][n] == null)
                _neural.Dic[_idx][n] = 0;

            // 为当前数字增加可能性
            if(_neural.Dic[_idx][n] >= 0)
            {
                _neural.Dic[_idx][n] += input.PreResult.A * 0.1 * (input.PreResult.A + input.PreResult.B);
                _neural.Dic[_idx][n] += input.PreResult.B * 0.05 * (input.PreResult.A + input.PreResult.B);
            }

            // 总数小于四，则其他值增加可能性
            if(input.PreResult.A + input.PreResult.B < 4)
            {
                for(int i = 0; i < 10; i++)
                {
                    if(i == n)
                        continue;

                    if(_neural.Dic[_idx][i] == null)
                        _neural.Dic[_idx][i] = 0;

                    if(_neural.Dic[_idx][i] >= 0)
                    {
                        //var currentValuePosible = _neural.Dic[_idx][n] == null || _neural.Dic[_idx][n] < 0 ? 0 : _neural.Dic[_idx][n];
                        var avgPosible = _neural.Dic[_idx].Values.Where(x => (x ?? 0) >= 0).Average();
                        _neural.Dic[_idx][i] = avgPosible + new Random(Guid.NewGuid().GetHashCode()).NextDouble() * (5 - input.PreResult.A - input.PreResult.B);
                    }
                }
            }

            var ps = Posibles;
            var rst = ps.Any() ? ps.First() : -1;
            return rst;
        }

        public void Except(IEnumerable<int> nums)
        {
            foreach(var n in nums)
            {
                var all = new List<int>();
                for(int i = 0; i < 4; i++)
                {
                    var p = _neural.Dic[i].Where(x => x.Value == null || x.Value >= 0).Select(x => x.Key);
                    if(i == _idx)
                        p = p.Except(nums);
                    all.AddRange(p);
                }
                Debug.Assert(all.Distinct().Count() >= 4);
                Debug.Assert(_neural.Dic[_idx].Where(x => x.Value == null || x.Value >= 0).Where(x => x.Key != n).Any());

                _neural.Dic[_idx][n] = -9999999;
            }
        }
    }
}
