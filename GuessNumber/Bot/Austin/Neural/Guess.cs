﻿namespace GuessNumber.Bot.Austin.Neural
{
    public class Guess
    {
        public AB PreResult;
        public int Number;
        public AB CurrentResult;

        public override string ToString() {
            return $"{PreResult} => {Number} => {CurrentResult}";
        }
    }
}
