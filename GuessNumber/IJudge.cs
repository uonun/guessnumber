﻿using System;
namespace GuessNumber
{
    interface IJudge
    {
        int Init();
        string Judge(int number);
        int JudgeCount { get; }
        int Answer { get; }
    }
}
