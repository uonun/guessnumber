﻿using System;
namespace GuessNumber
{
    interface IPlayer
    {
        int Guess(string judgeResult);
    }
}
